/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.jdeployer.common;

import static org.junit.Assert.*;
/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class TestDeploymentArchiveController {

    @org.junit.Test
    public void deploymentArchiveControllerTest() {
        DeploymentArchiveController controller = new DeploymentArchiveController();
        assertTrue(controller.fileUpload("deployment-archive.dar").size()>1);
    }
}
