/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.common;

import com.jdeployer.api.DeploymentArchive;
import com.jdeployer.api.DeploymentResult;
import com.jdeployer.api.DeploymentService;
import com.jdeployer.api.message.ErrorMessage;
import com.jdeployer.api.message.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <description>.
 *
 * @author mcmartins
 */
public class DeploymentArchiveController {

    private static final Log LOGGER = LogFactory.getLog(DeploymentArchiveController.class);

    /**
     * File upload.
     */
    @SuppressWarnings("unchecked")
    public final List<Message> fileUpload(final String fileName) {
        final List<Message> resultList = new ArrayList<Message>();
        DeploymentResult<Message> result = null;
        try {
            final File file = new File(fileName);

            //generate a DeploymentArchive from a file
            final DeploymentArchive dar = new DeploymentArchiveImpl(file, file.getName());

            //ideally it should be a singleton
            final DeploymentService deploymentService = new DeploymentServiceImpl();

            //execute de chain
            result = deploymentService.deploy(dar);

        } catch (final Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            resultList.add(new ErrorMessage(e.getLocalizedMessage()));
        }

        if (result != null) {
            resultList.addAll(result.getMessages());
        }

        return resultList;
    }

    /**
     * File upload.
     */
    @SuppressWarnings("unchecked")
    public final List<Message> fileUpload(final InputStream darStream, final String fileName) {
        final List<Message> resultList = new ArrayList<Message>();
        DeploymentResult<Message> result = null;
        try {
            //generate a DeploymentArchive from a file
            final DeploymentArchive dar = new DeploymentArchiveImpl(darStream, fileName);
            //generate a DeploymentArchive from a stream

            //ideally it should be a singleton
            final DeploymentService deploymentService = new DeploymentServiceImpl();

            //execute de chain
            result = deploymentService.deploy(dar);

        } catch (final Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            resultList.add(new ErrorMessage(e.getLocalizedMessage()));
        }

        if (result != null) {
            resultList.addAll(result.getMessages());
        }

        return resultList;
    }
}
