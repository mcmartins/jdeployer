/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.common;

import com.jdeployer.api.DeploymentArchive;
import com.jdeployer.api.DeploymentResult;
import com.jdeployer.api.DeploymentService;
import com.jdeployer.api.message.ErrorMessage;
import com.jdeployer.api.message.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * <description>.
 *
 * @author mcmartins
 */
public class DeploymentServiceImpl implements DeploymentService<Message> {

    private static final Log LOGGER = LogFactory.getLog(DeploymentServiceImpl.class);

    @Override
    public DeploymentResult<Message> deploy(final DeploymentArchive deploymentArchive) {
        if (!deploymentArchive.isLastVersion()) {
            throw new RuntimeException("Not a valid archive version!");
        }

        final List<Message> messages = new ArrayList<Message>();
        DeploymentBaseCatalogLoader deploymentLoader = null;

        try {
            //create the catalog
            deploymentLoader = new DeploymentBaseCatalogLoader(deploymentArchive);
            //execute the chain
            deploymentLoader.execute();
            //add messages from commands execution
            messages.addAll(deploymentLoader.getDeploymentContext().getMessages());
        } catch (final Exception e) {
            //on a transactionable environment you could force rollback here
            String lastExecutedCommand = null;
            if (deploymentLoader != null) {
                lastExecutedCommand = deploymentLoader.getDeploymentContext().getLastExecutedCommand();
            }
            final String message = MessageFormat.format(
                    "[Chain finished on command - {0} - , with the following error - {1} - ]",
                    lastExecutedCommand, e.getLocalizedMessage());
            //add to result messages the cause error message
            messages.add(new ErrorMessage(message));
            LOGGER.error(message, e);
        }

        return new DeploymentResult<Message>() {

            @Override
            @SuppressWarnings("unchecked")
            public List<Message> getMessages() {
                return messages;
            }
        };
    }
}
