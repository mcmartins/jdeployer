/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.common;

import com.google.common.base.Preconditions;
import com.jdeployer.api.DeploymentArchive;
import com.jdeployer.api.context.DeploymentContext;
import com.jdeployer.api.message.Message;
import com.jdeployer.api.util.FileUtils;
import org.apache.commons.chain.Catalog;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.apache.commons.chain.config.ConfigParser;
import org.apache.commons.chain.impl.CatalogFactoryBase;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Handles deployment based on Catalog File.
 *
 * @author mcmartins
 */
public class DeploymentBaseCatalogLoader {

    private static final String UNCHECKED = "unchecked";

    private final DeploymentContext context;
    private final Catalog catalog;
    private final Command command;

    /**
     * Default Constructor
     * <p/>
     *
     * @param deploymentArchive the {@link com.jdeployer.api.DeploymentArchive} object
     * @throws Exception if deployment configuration could not be loaded.
     */
    @SuppressWarnings(UNCHECKED)
    public DeploymentBaseCatalogLoader(final DeploymentArchive deploymentArchive) throws Exception {
        Preconditions.checkNotNull(deploymentArchive);
        Preconditions.checkNotNull(deploymentArchive.getDeploymentArchiveChainFileConfiguration());
        Preconditions.checkNotNull(deploymentArchive.getDeploymentArchiveFileConfiguration());

        final InputStream chainConfigStream = deploymentArchive.getDeploymentArchiveChainFileConfiguration();
        final ConfigParser parser = new ConfigParser();
        parser.parse(FileUtils.inputStreamToURL(chainConfigStream));
        this.catalog = CatalogFactoryBase.getInstance()
                .getCatalog(deploymentArchive.getDeploymentArchiveFileConfiguration().getConfigurationCatalogName());
        this.command = this.catalog.getCommand(deploymentArchive.getDeploymentArchiveFileConfiguration().getConfigurationChainName());
        this.context = new DeploymentContext();
        this.context.put(DeploymentArchive.DEPLOYMENT_ARCHIVE, deploymentArchive);
        this.context.put(DeploymentArchive.DEPLOYMENT_ARCHIVE_LAST_COMMAND_EXECUTED, null);
        this.context.put(DeploymentArchive.DEPLOYMENT_ARCHIVE_RESULT, new ArrayList<Message>());
    }

    /**
     * Execute the chain.
     *
     * @throws Exception if any error occurs.
     */
    public void execute() throws Exception {
        this.command.execute(this.context);
    }

    /**
     * Returns the catalog for the application deployment.
     *
     * @return {@link Catalog} the catalog.
     * @throws Exception if resource could not be load.
     */
    public Catalog getCatalog() throws Exception {
        return this.catalog;
    }

    /**
     * Returns the Context for the application deployment.
     *
     * @return {@link Context} the context.
     */
    public DeploymentContext getDeploymentContext() {
        return this.context;
    }
}
