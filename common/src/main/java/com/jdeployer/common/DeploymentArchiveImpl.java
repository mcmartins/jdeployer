/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.common;

import com.google.common.base.Preconditions;
import com.java.reuse.core.common.Codable;
import com.jdeployer.api.ArchiveHelper;
import com.jdeployer.api.DeploymentArchive;
import com.jdeployer.api.DeploymentArchiveFile;
import com.jdeployer.api.configuration.DeployerConfiguration;
import com.jdeployer.api.configuration.DeployerConfigurationImpl;
import com.jdeployer.api.exception.DeploymentChainNotFoundException;
import com.jdeployer.api.file.FolderElement;
import com.jdeployer.api.jaxb.JAXBUtil;
import com.jdeployer.generated.DeploymentArchiveConfiguration;
import org.apache.commons.io.IOUtils;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Concrete implementation for {@link com.jdeployer.api.DeploymentArchive} specification.
 *
 * @author mcmartins
 */
public class DeploymentArchiveImpl implements DeploymentArchive {

    private final DeploymentArchiveFile<FolderElement> deploymentArchiveFile;
    private DeploymentArchiveConfiguration deploymentArchiveConfiguration;
    private String lastExecutedCommand;

    /**
     * Default constructor.
     *
     * @param file     the Deployment Archive file inputStream.
     * @param fileName the file name.
     * @throws IOException   if file does not exists.
     * @throws JAXBException if invalid xml was provided.
     */
    public DeploymentArchiveImpl(final InputStream file, final String fileName)
            throws IOException, JAXBException {
        Preconditions.checkNotNull(file);
        Preconditions.checkNotNull(fileName);

        this.deploymentArchiveFile = DeploymentArchiveFileImpl.createDeploymentArchiveFile(file, fileName);
        this.tryLoadDeploymentArchiveFileConfiguration();
    }

    public DeploymentArchiveImpl(final File file, final String fileName)
            throws IOException, JAXBException {
        this(new FileInputStream(file), fileName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLastVersion() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeploymentArchiveFile getDeploymentArchiveFile() {
        return this.deploymentArchiveFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLastExecutedCommand() {
        return this.lastExecutedCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLastExecutedCommand(final String name) {
        this.lastExecutedCommand = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeploymentArchiveConfiguration getDeploymentArchiveFileConfiguration() {
        return this.deploymentArchiveConfiguration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getDeploymentArchiveChainFileConfiguration() throws JAXBException {
        return this.tryLoadDeploymentArchiveChainFileConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArchiveHelper getDeploymentArchive(final String deploymentCommandName) {
        Preconditions.checkNotNull(deploymentCommandName);

        return this.loadArchive(deploymentCommandName);
    }


    /**
     * Loads configuration from archive.
     *
     * @throws JAXBException if invalid xml was found.
     */
    private void tryLoadDeploymentArchiveFileConfiguration() throws JAXBException {
        if (this.deploymentArchiveConfiguration == null) {
            final InputStream stream = this.deploymentArchiveFile.getRootFolderFileContent(
                    DeploymentArchiveFile.DEPLOYMENT_ARCHIVE_CONFIGURATION_FILE_NAME);
            this.deploymentArchiveConfiguration = JAXBUtil.unmarshal(stream, DeploymentArchiveConfiguration.class);
        }
    }

    /**
     * Loads configuration from archive.
     *
     * @return {@link InputStream} the configuration input stream.
     * @throws JAXBException if invalid xml was found.
     */
    protected InputStream tryLoadDeploymentArchiveChainFileConfiguration() throws JAXBException {
        String config;
        final DeployerConfiguration configurationService = new DeployerConfigurationImpl();

        config = configurationService.get(new Codable<String>() {

            @Override
            public String getCode() {
                return DeploymentArchiveFile.DEPLOYMENT_ARCHIVE_CONFIGURATION_KEY;
            }
        }, String.class);

        if (config == null) {
            //chain must exists
            throw new DeploymentChainNotFoundException();
        }

        return IOUtils.toInputStream(config);
    }

    /**
     * Resolves a {@link DeploymentArchiveConfiguration.Archive} based on deployment step name.
     *
     * @param deploymentStepName the step name.
     * @return {@link DeploymentArchiveConfiguration.Archive}.
     */
    private ArchiveHelper loadArchive(final String deploymentStepName) {
        DeploymentArchiveConfiguration.Archive result = null;
        for (final DeploymentArchiveConfiguration.Archive archive : this.deploymentArchiveConfiguration.getArchive()) {
            if (archive.getDeploymentCommandName().equals(deploymentStepName)) {
                result = archive;
                break;
            }
        }
        return result == null ? null : new ArchiveHelperImpl(result, this.deploymentArchiveFile);
    }
}
