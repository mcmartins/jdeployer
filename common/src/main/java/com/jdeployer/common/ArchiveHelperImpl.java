/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.common;

import com.google.common.base.Preconditions;
import com.jdeployer.api.ArchiveHelper;
import com.jdeployer.api.DeploymentArchive;
import com.jdeployer.api.DeploymentArchiveFile;
import com.jdeployer.api.file.FolderElement;
import com.jdeployer.api.jaxb.JAXBUtil;
import com.jdeployer.generated.ArchiveConfiguration;
import com.jdeployer.generated.DeploymentArchiveConfiguration;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.InputStream;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class ArchiveHelperImpl implements ArchiveHelper {

    private static final String UNCHECKED = "unchecked";

    private final DeploymentArchiveConfiguration.Archive archive;
    private final DeploymentArchiveFile<FolderElement> deploymentArchiveFile;

    private DeploymentArchive.ArchiveType type;
    private Document configuration;
    private String archiveName;

    public ArchiveHelperImpl(final DeploymentArchiveConfiguration.Archive archive,
                             final DeploymentArchiveFile<FolderElement> deploymentArchiveFile) {
        this.archive = Preconditions.checkNotNull(archive);
        this.deploymentArchiveFile = Preconditions.checkNotNull(deploymentArchiveFile);
        this.resolveType();
        this.resolveConfiguration();
    }

    private void resolveConfiguration() {
        final ArchiveConfiguration archiveConfiguration;
        if (this.archive.isSetCompressed()) {
            archiveConfiguration = this.archive.getCompressed().getArchiveConfiguration();
        } else if (this.archive.isSetFolder()) {
            archiveConfiguration = this.archive.getFolder().getArchiveConfiguration();
        } else if (this.archive.isSetFile()) {
            archiveConfiguration = this.archive.getFile().getArchiveConfiguration();
        } else if (this.archive.isSetNone()) {
            archiveConfiguration = this.archive.getNone().getArchiveConfiguration();
        } else {
            archiveConfiguration = null;
        }

        final ArchiveConfiguration.Configuration configuration;
        if (archiveConfiguration != null) {
            configuration = archiveConfiguration.getConfiguration();
            try {
                this.configuration = JAXBUtil.createDocumentFromAnyType(configuration.getAny());
            } catch (final ParserConfigurationException e) {
                throw new RuntimeException("Could not unmarshall configuration!");
            }
        }
    }

    private void resolveType() {
        if (this.archive.isSetCompressed()) {
            this.type = DeploymentArchive.ArchiveType.COMPRESSED;
            this.archiveName = this.archive.getCompressed().getName();
        } else if (this.archive.isSetFolder()) {
            this.type = DeploymentArchive.ArchiveType.FOLDER;
            this.archiveName = this.archive.getFolder().getName();
        } else if (this.archive.isSetFile()) {
            this.type = DeploymentArchive.ArchiveType.FILE;
            this.archiveName = this.archive.getFile().getName();
        } else if (this.archive.isSetNone()) {
            this.type = DeploymentArchive.ArchiveType.NONE;
            this.archiveName = this.archive.getNone().getName();
        } else {
            this.type = DeploymentArchive.ArchiveType.NONE;
            this.archiveName = null;
        }

        Preconditions.checkNotNull(this.archiveName);
    }

    @Override
    public <T> T getConfiguration(final Class<T> type, final String xsdName) {
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(xsdName);

        T result = null;
        if (this.configuration != null) {
            try {
                result = JAXBUtil.unmarshal(this.configuration, type);
            } catch (final JAXBException e) {
                throw new RuntimeException("Could not unmarshall configuration!");
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return this.type == DeploymentArchive.ArchiveType.FILE
                || this.type == DeploymentArchive.ArchiveType.NONE ? this.archiveName : null;
    }

    @Override
    public String getPath() {
        return this.type == DeploymentArchive.ArchiveType.FOLDER
                || this.type == DeploymentArchive.ArchiveType.COMPRESSED ? this.archiveName : null;
    }

    @Override
    @SuppressWarnings(UNCHECKED)
    public <T> T getFolder() {
        return this.type == DeploymentArchive.ArchiveType.FOLDER
                ? (T) this.deploymentArchiveFile.getFolder(this.getPath()) : null;
    }

    @Override
    public InputStream getContentStream() {
        return this.type == DeploymentArchive.ArchiveType.COMPRESSED
                || this.type == DeploymentArchive.ArchiveType.FILE
                ? deploymentArchiveFile.getFileContent(archiveName) : null;
    }

    @Override
    public InputStream getContentStream(final String name) {
        return this.type == DeploymentArchive.ArchiveType.COMPRESSED
                || this.type == DeploymentArchive.ArchiveType.FOLDER
                ? deploymentArchiveFile.getFileInFolderContent(this.getPath(), name) : null;
    }
}
