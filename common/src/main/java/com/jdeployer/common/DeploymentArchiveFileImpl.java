/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.common;

import com.google.common.base.Preconditions;
import com.jdeployer.api.DeploymentArchiveFile;
import com.jdeployer.api.file.FileElement;
import com.jdeployer.api.file.FolderElement;
import com.jdeployer.api.util.ExceptionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Concrete implementation for {@link com.jdeployer.api.DeploymentArchiveFile} specification.
 *
 * @author mcmartins
 */
public class DeploymentArchiveFileImpl implements DeploymentArchiveFile<FolderElement> {

    /**
     * {@link com.jdeployer.api.file.FolderElement}
     */
    private final FolderElement deploymentArchiveFileContent;

    /**
     * {@link InputStream}
     */
    private final InputStream deploymentArchiveFile;

    /**
     * Default constructor.
     *
     * @param deploymentArchiveContentElement
     *             the {@link com.jdeployer.api.file.FolderElement}
     * @param file the {@link InputStream}
     */
    public DeploymentArchiveFileImpl(final FolderElement deploymentArchiveContentElement, final InputStream file) {
        Preconditions.checkNotNull(deploymentArchiveContentElement);
        Preconditions.checkNotNull(file);

        this.deploymentArchiveFileContent = deploymentArchiveContentElement;
        this.deploymentArchiveFile = file;
    }

    /**
     * Creates a Deployment Archive File based on an {@link java.util.zip.ZipInputStream}.
     *
     * @param fileInputStream           the file input.
     * @param deploymentArchiveFileName the file name
     * @return {@code deploymentArchiveFileContent} value
     * @throws java.io.IOException if file does not exists
     */
    public static DeploymentArchiveFile<FolderElement> createDeploymentArchiveFile(final InputStream fileInputStream, final String deploymentArchiveFileName) throws IOException {
        Preconditions.checkNotNull(fileInputStream);

        final ZipInputStream deploymentArchiveFileInputStream = new ZipInputStream(fileInputStream);

        final FolderElement deploymentArchiveContentElement = new FolderElement(deploymentArchiveFileName);
        final DeploymentArchiveFile<FolderElement> deploymentArchiveFile = new DeploymentArchiveFileImpl(deploymentArchiveContentElement, fileInputStream);
        ZipEntry entry;
        while ((entry = deploymentArchiveFileInputStream.getNextEntry()) != null) {
            final String name = entry.getName();
            final List<String> parsedName = Arrays.asList(name.split("/"));
            final List<String> pathResult = new ArrayList<String>(parsedName);
            if (entry.isDirectory()) {
                FolderElement.insertSententialPath(deploymentArchiveContentElement, pathResult);
            } else {
                final byte[] fileContent = IOUtils.toByteArray(deploymentArchiveFileInputStream);
                final FileElement fileElement = new FileElement(parsedName.get(parsedName.size() - 1),
                        fileContent);
                if (pathResult.size() == 1) {
                    deploymentArchiveContentElement.getChildFileElements().add(fileElement);
                } else {
                    final FolderElement resultContentElement = FolderElement.insertSententialPath(
                            deploymentArchiveContentElement, pathResult.subList(0, pathResult.size() - 1));
                    resultContentElement.getChildFileElements().add(fileElement);
                }
            }
        }

        return deploymentArchiveFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFileName() {
        return this.deploymentArchiveFileContent.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getFileStream() {
        return this.deploymentArchiveFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FolderElement getFileContent() {
        return this.deploymentArchiveFileContent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getRootFolderFileContent(final String fileName) {
        Preconditions.checkNotNull(fileName);

        return new ByteArrayInputStream(this.getFileContent().getFile(fileName).getFileElementContent());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getFileInFolderContent(final String path, final String fileName) {
        Preconditions.checkNotNull(path);
        Preconditions.checkNotNull(fileName);

        final FolderElement folder = this.getFolderContentHelper(path);
        final FileElement element = folder.getFile(fileName);
        ExceptionUtils.throwIfObjectIsNull(element, fileName);

        return new ByteArrayInputStream(element.getFileElementContent());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FolderElement getFolder(final String location) {
        Preconditions.checkNotNull(location);

        final String[] pathTokens = StringUtils.split(location, '/');

        final Iterator<String> it = Arrays.asList(pathTokens).iterator();

        FolderElement currentFolder = this.getRootFolderContentHelper();

        while (it.hasNext() && (currentFolder != null)) {
            final String folderName = it.next();
            currentFolder = currentFolder.getFolder(folderName);
        }

        return currentFolder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getFileContent(final String fileName) {
        Preconditions.checkNotNull(fileName);

        return new ByteArrayInputStream(this.getFileContentHelper(fileName).getFileElementContent());
    }

    /**
     * Returns the Folder Content based on a path.
     *
     * @param path the path to the folder.
     * @return folderContent.
     */
    private FolderElement getFolderContentHelper(final String path) {
        Preconditions.checkNotNull(path);

        final FolderElement element = this.getRootFolderContentHelper().getFolder(path);
        ExceptionUtils.throwIfObjectIsNull(element, path);

        return element;
    }

    /**
     * Returns the File Content based on a fileName.
     *
     * @param fileName the file name.
     * @return fileContent.
     */
    private FileElement getFileContentHelper(final String fileName) {
        Preconditions.checkNotNull(fileName);

        final FileElement element = this.getRootFolderContentHelper().getFile(fileName);
        ExceptionUtils.throwIfObjectIsNull(element, fileName);

        return element;
    }

    /**
     * Returns the Root Folder Content.
     *
     * @return rootFolderContent.
     */
    private FolderElement getRootFolderContentHelper() {
        final FolderElement element = this.getFileContent().getFolder(DEPLOYMENT_ARCHIVE_FILE_ROOT_FOLDER);
        ExceptionUtils.throwIfObjectIsNull(element, DEPLOYMENT_ARCHIVE_FILE_ROOT_FOLDER);
        return element;
    }
}
