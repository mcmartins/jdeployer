/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.configuration;

import com.java.reuse.core.common.Codable;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class DeployerConfigurationImpl implements DeployerConfiguration {

    private static final Log LOGGER = LogFactory.getLog(DeployerConfigurationImpl.class);

    private static final String CONFIGURATION_FILE = "configuration/deployer-configuration.properties";

    private CompositeConfiguration configuration;

    private static final DeployerConfiguration deployerConfiguration = new DeployerConfigurationImpl();

    public static DeployerConfiguration getInstance() {
        return deployerConfiguration;
    }

    /**
     * Default constructor. Initializes the configuration chain with a set of default configurations from a properties file.
     */
    public DeployerConfigurationImpl() {
        this.configuration = new CompositeConfiguration();
        try {
            this.configuration.addConfiguration(new PropertiesConfiguration(CONFIGURATION_FILE));
        } catch (final ConfigurationException e) {
            LOGGER.warn("Could not load default configurations, hope you provide your own configuration...", e);
        }
    }

    @Override
    public void addConfiguration(final Configuration configuration) {
        this.configuration.addConfiguration(configuration);
        this.dealWithDefaultConfiguration();
    }

    @Override
    public void addPropertiesConfiguration(final String classPathPropertiesConfigurationFile) throws ConfigurationException {
        this.configuration.addConfiguration(new PropertiesConfiguration(classPathPropertiesConfigurationFile));
        this.dealWithDefaultConfiguration();
    }

    /**
     * Handles the position of default configuration on the configurations hierarchy.
     * Default configuration shall be the last, in order to be used only if no property exists in the configurations
     * provided by the developer.
     */
    private void dealWithDefaultConfiguration() {
        //in order to use the provided configurations and maintain the default configurations, the default shall be the last configuration in the list
        final Configuration defaultConfiguration =
                this.configuration.getConfiguration(this.configuration.getNumberOfConfigurations() - 1);
        //remove it
        this.configuration.removeConfiguration(defaultConfiguration);
        //add it to the end of the list
        this.configuration.addConfiguration(defaultConfiguration);
    }

    @Override
    public <T> T get(final Codable<String> property, final Class<T> type) {
        return type.cast(this.configuration.getProperty(property.getCode()));
    }

    @Override
    public Object get(final Codable<String> property) {
        return this.configuration.getProperty(property.getCode());
    }

    @Override
    public void add(final Codable<String> property, final Object value) {
        this.configuration.addProperty(property.getCode(), value);
    }
}
