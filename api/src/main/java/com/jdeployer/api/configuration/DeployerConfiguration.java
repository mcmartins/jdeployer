/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.configuration;

import com.java.reuse.core.common.Codable;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;

/**
 * Configuration definition.
 *
 * @author Manuel Martins
 */
public interface DeployerConfiguration {

    /**
     * Adds a new {@code Configuration} source to use within the jdeployer.
     *
     * @param configuration the configuration to add.
     */
    void addConfiguration(Configuration configuration);

    /**
     * Adds a new {@code PropertiesConfiguration} source to use within the jdeployer.
     *
     * @param classPathPropertiesConfigurationFile
     *         the the class path of the properties configuration file to add.
     */
    void addPropertiesConfiguration(String classPathPropertiesConfigurationFile) throws ConfigurationException;

    /**
     * Returns a property from the chain of configurations casted to its type.
     *
     * @param property the property key to retrieve.
     * @param type     the class type of the value for cast.
     * @param <T>      the type of the value.
     * @return the resolved property.
     */
    <T> T get(Codable<String> property, Class<T> type);

    /**
     * Returns a property from the chain of configurations.
     *
     * @param property the property key to retrieve.
     * @return the resolved property.
     */
    Object get(Codable<String> property);

    /**
     * Adds a property value to the chain of configurations.
     *
     * @param property the property key to retrieve.
     * @param value    the value.
     */
    void add(Codable<String> property, Object value);

}
