/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.configuration;

import com.java.reuse.core.common.Codable;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public enum ConfigurationCodes implements Codable<String> {

    DEFAULT_XML_SCHEMA_PATH("deployment.default.xml-schemas.path");

    private final String code;

    /**
     * Initializes a new instance of {@code Codes} with the specified error code.
     *
     * @param code The error code for this enumeration value.
     */
    ConfigurationCodes(final String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}

