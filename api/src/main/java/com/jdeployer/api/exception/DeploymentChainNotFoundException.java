/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.exception;

import com.java.reuse.core.exception.api.AbstractRuntimeException;
import com.jdeployer.api.resource.ResourceCodes;

/**
 * Exception occurs when the Deployment chain does not exists on the database.
 *
 * @author mcmartins
 */
public class DeploymentChainNotFoundException extends AbstractRuntimeException {

    /**
     * Default constructor.
     *
     * @param cause the throwable cause.
     */
    public DeploymentChainNotFoundException(final Throwable cause) {
        super(ResourceCodes.CHAIN_CONFIGURATION_NOT_FOUND, cause);
    }

    /**
     * Default constructor.
     */
    public DeploymentChainNotFoundException() {
        this(null);
    }
}
