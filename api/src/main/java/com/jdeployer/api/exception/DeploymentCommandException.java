/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.exception;

import com.java.reuse.core.exception.api.AbstractCheckedException;
import com.jdeployer.api.resource.ResourceCodes;

/**
 * Exception thrown by commands implementation.
 *
 * @author mcmartins
 */
public class DeploymentCommandException extends AbstractCheckedException {

    /**
     * Default constructor.
     *
     * @param cause         the exception cause.
     * @param commandName   the command name.
     * @param typeOfArchive the type of archive
     * @param archiveType   the archive type.
     */
    public DeploymentCommandException(final Throwable cause, final String commandName, final String typeOfArchive, final String archiveType) {
        super(ResourceCodes.COMMAND_CONFIGURATION_NOT_FOUND, cause);
        this.addParameters(commandName);
        this.addParameters(typeOfArchive);
        this.addParameters(archiveType);
    }
}
