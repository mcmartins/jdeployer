/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.jdeployer.api.jaxb;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class SchemaConfiguration extends AbstractConfiguration {

    public static final String SCHEMAS_KEY = "jdeployer.registered.schemas";

    private static final String UNCHECKED = "unchecked";

    private final Map<String, Object> store = new LinkedHashMap<String, Object>();

    @Override
    protected void addPropertyDirect(String key, Object value) {
        Object previousValue = getProperty(SCHEMAS_KEY);

        if (previousValue == null) {
            store.put(SCHEMAS_KEY, value);
        } else if (previousValue instanceof List) {
            // safe to case because we have created the lists ourselves
            @SuppressWarnings(UNCHECKED)
            List<Object> valueList = (List<Object>) previousValue;
            // the value is added to the existing list
            valueList.add(value);
        } else {
            // the previous value is replaced by a list containing the previous value and the new value
            List<Object> list = new ArrayList<Object>();
            list.add(previousValue);
            list.add(value);

            store.put(SCHEMAS_KEY, list);
        }
    }

    @Override
    public Object getProperty(String key) {
        return store.get(SCHEMAS_KEY);
    }

    @Override
    public boolean isEmpty() {
        return store.isEmpty();
    }

    @Override
    public boolean containsKey(String key) {
        return store.containsKey(SCHEMAS_KEY);
    }

    @Override
    protected void clearPropertyDirect(String key) {
        if (containsKey(key)) {
            store.remove(key);
        }
    }

    @Override
    public void clear() {
        fireEvent(EVENT_CLEAR, null, null, true);
        store.clear();
        fireEvent(EVENT_CLEAR, null, null, false);
    }

    @Override
    public Iterator<String> getKeys() {
        return store.keySet().iterator();
    }

    public String getPackagesFromSchemas() {
        final Iterator<String> it = this.store.keySet().iterator();
        final StringBuilder builder = new StringBuilder();
        String pack;
        String clazz;
        for (; it.hasNext(); ) {
            clazz = it.next();
            pack = ClassUtils.getPackageName(clazz);
            if (StringUtils.isNotEmpty(pack)) {
                builder.append(pack);
                if (it.hasNext()) {
                    builder.append(":");
                }
            }
        }
        return builder.toString();
    }
}
