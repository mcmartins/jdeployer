/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.jaxb;

import com.google.common.base.Preconditions;
import com.jdeployer.api.configuration.ConfigurationCodes;
import com.jdeployer.api.configuration.DeployerConfigurationImpl;
import com.jdeployer.api.util.FileUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Provides methods to marshal Java objects to XML and to unmarshal XML to Java objects.
 *
 * @author : mcmartins
 */
public final class JAXBUtil {

    public static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    private static final Log LOG = LogFactory.getLog(JAXBUtil.class);

    private static Map<String, JAXBContext> contextMap = new HashMap<String, JAXBContext>();
    private Map<String, Schema> xmlSchemas = new HashMap<String, Schema>();

    private JAXBUtil() {
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        //JAXBUtil.unmarshal(inputStream, DeploymentArchiveConfiguration.Archive.class, getPackagesFromSchemas());
        List<URL> files = null;
        try {
            files = FileUtils.loadAllResourcesFromDirectoryInClassPath(classLoader,
                    DeployerConfigurationImpl.getInstance().get(ConfigurationCodes.DEFAULT_XML_SCHEMA_PATH,
                            String.class), ".xsd");
        } catch (URISyntaxException e) {

        } catch (IOException e) {

        }
    }

    public String getPackagesFromSchemas() {
        final Iterator<String> it = this.xmlSchemas.keySet().iterator();
        final StringBuilder builder = new StringBuilder();
        String pack;
        String clazz;
        for (; it.hasNext(); ) {
            clazz = it.next();
            pack = ClassUtils.getPackageName(clazz);
            if (StringUtils.isNotEmpty(pack)) {
                builder.append(pack);
                if (it.hasNext()) {
                    builder.append(":");
                }
            }
        }
        return builder.toString();
    }

    public static <T> T unmarshal(final Document document, final Class<T> type) throws JAXBException {
        return unmarshal(document, type, null);
    }

    public static <T> T unmarshal(final Node node, final Class<T> type, final String jaxbContextPath)
            throws JAXBException {
        return unmarshal(node, type, jaxbContextPath, null);
    }

    public static <T> T unmarshal(final Node node, final Class<T> type, final String jaxbContextPath,
                                  final Object objectFactory) throws JAXBException {
        String jaxbContextPathFinal = jaxbContextPath;
        if (jaxbContextPathFinal == null) {
            jaxbContextPathFinal = type.getPackage().getName();
        }
        final Unmarshaller unmarshaller = getJaxbContext(jaxbContextPathFinal).createUnmarshaller();
        setObjectFactory(unmarshaller, objectFactory);
        setSchema(unmarshaller, type);
        final JAXBElement<?> result = unmarshaller.unmarshal(node, type);
        LOG.debug(MessageFormat.format("Casting result from {0} to {1}", result.getValue().getClass(), type));
        return type.cast(result.getValue());
    }

    public static <T> T unmarshal(final InputStream inputStream, final Class<T> type) throws JAXBException {
        return unmarshal(inputStream, type, null);
    }

    public static <T> T unmarshal(final InputStream inputStream, final Class<T> type, final String jaxbContextPath)
            throws JAXBException {
        return unmarshal(inputStream, type, jaxbContextPath, null);
    }

    public static <T> T unmarshal(final InputStream inputStream, final Class<T> type, final String jaxbContextPath,
                                  final Object objectFactory) throws JAXBException {
        String jaxbContextPathFinal = jaxbContextPath;
        if (jaxbContextPathFinal == null) {
            jaxbContextPathFinal = type.getPackage().getName();
        }
        final Unmarshaller unmarshaller = getJaxbContext(jaxbContextPathFinal).createUnmarshaller();
        setObjectFactory(unmarshaller, objectFactory);
        setSchema(unmarshaller);
        final Object result = unmarshaller.unmarshal(inputStream);
        LOG.debug(MessageFormat.format("Casting result from {0} to {1}", result.getClass(), type));
        return type.cast(result);
    }

    private static void setSchema(final Unmarshaller unmarshaller) {
        try {
            unmarshaller.setSchema(getSchema());
        } catch (final SAXException e) {
            LOG.error(e);
        }
    }

    private static <T> void setSchema(final Unmarshaller unmarshaller, final Class<T> type) {
        setSchema(unmarshaller);
    }

    public static Schema getSchema() throws SAXException {
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA);
        final URL resourceURL = classLoader.getResource("schema/deployment-archive-configuration.xsd");

        return schemaFactory.newSchema(resourceURL);
    }

    private static void setObjectFactory(final Unmarshaller unmarshaller, final Object objectFactory) {
        if (objectFactory != null) {
            try {
                unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory", objectFactory);
            } catch (final PropertyException e) {
                try {
                    unmarshaller.setProperty("com.sun.xml.internal.bind.ObjectFactory", objectFactory);
                } catch (final PropertyException e1) {
                    LOG.debug(e1.getMessage(), e1);
                }
            }
        }
    }

    private static synchronized JAXBContext getJaxbContext(final String jaxbContextPath) throws JAXBException {
        JAXBContext jaxbContext = contextMap.get(jaxbContextPath);
        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(jaxbContextPath);
            contextMap.put(jaxbContextPath, jaxbContext);
        }
        return jaxbContext;
    }


    /**
     * Creates a Document based on a ANY element type.
     *
     * @param configuration the list of element of any type.
     * @return {@link Document}
     * @throws javax.xml.parsers.ParserConfigurationException
     *          if a parser error occurs.
     */
    public static Document createDocumentFromAnyType(final List<Element> configuration)
            throws ParserConfigurationException {
        Preconditions.checkNotNull(configuration);

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        final DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        final Document doc = documentBuilder.newDocument();
        final Element rootElement = (Element) doc.importNode(configuration.get(0), true);
        doc.appendChild(rootElement);
        return doc;
    }
}
