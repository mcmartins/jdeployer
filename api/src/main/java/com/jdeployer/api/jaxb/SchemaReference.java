/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.jdeployer.api.jaxb;

import org.xml.sax.SAXException;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.net.URL;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class SchemaReference {

    private final ClassLoader classLoader;
    private final String resourcePath;
    private final Class<?> type;
    private Schema schema;

    public SchemaReference(final ClassLoader classLoader, final String resourcePath) {
        this.type = null;
        this.classLoader = classLoader;
        this.resourcePath = resourcePath;
    }

    public SchemaReference(final ClassLoader classLoader, final String resourcePath, final Class<?> type) {
        this.type = type;
        this.classLoader = classLoader;
        this.resourcePath = resourcePath;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public Class<?> getType() {
        return type;
    }

    public Schema getSchema() throws SAXException {
        if (this.schema == null) {
            final SchemaFactory schemaFactory = SchemaFactory.newInstance(JAXBUtil.W3C_XML_SCHEMA);
            final URL resourceURL = this.classLoader.getResource(this.resourcePath);
            this.schema = schemaFactory.newSchema(resourceURL);
        }
        return this.schema;
    }
}
