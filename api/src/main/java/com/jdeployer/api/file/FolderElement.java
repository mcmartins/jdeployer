/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.file;

import com.jdeployer.api.util.ExceptionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Folder Element. Contains the Folder name, Folder child files and Folder child Folders.
 *
 * @author mcmartins
 */
public class FolderElement {

    private final String name;
    private final List<FolderElement> childFolderElements;
    private final List<FileElement> childFileElements;

    public FolderElement(final String name) {
        this.name = name;
        this.childFolderElements = new ArrayList<FolderElement>();
        this.childFileElements = new ArrayList<FileElement>();
    }

    /**
     * Gets {@code name} property.
     *
     * @return {@code name} value
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets {@code childFolderElements} property.
     *
     * @return {@code childFolderElements} value
     */
    public List<FolderElement> getChildFolderElements() {
        return this.childFolderElements;
    }

    /**
     * Sets {@code childFolderElements} property.
     *
     * @param childFoldersElements value to set
     */
    public void addChildFolderElements(final List<FolderElement> childFoldersElements) {
        this.childFolderElements.addAll(childFoldersElements);
    }

    /**
     * Gets {@code childFileElements} property.
     *
     * @return {@code childFileElements} value
     */
    public List<FileElement> getChildFileElements() {
        return this.childFileElements;
    }

    /**
     * Sets {@code childFileElements} property.
     *
     * @param childFileElements value to set
     */
    public void addChildFileElements(final List<FileElement> childFileElements) {
        this.childFileElements.addAll(childFileElements);
    }

    /**
     * Verify if the path exists on this content element, if not creates the path.
     *
     * @param rootElement .
     * @param path        .
     * @return {@link FolderElement} .
     */
    public static FolderElement insertSententialPath(final FolderElement rootElement, final List<String> path) {
        final FolderElement currentElement = new FolderElement(path.get(0));

        FolderElement elementToInsert = null;
        for (final FolderElement childContentElement : rootElement.getChildFolderElements()) {
            if (childContentElement.getName().equals(path.get(0))) {
                elementToInsert = childContentElement;
                break;
            }
        }

        if (elementToInsert == null) {
            elementToInsert = currentElement;
            rootElement.getChildFolderElements().add(elementToInsert);
        }

        final List<String> subPath = path.subList(1, path.size());
        if (subPath.size() > 0) {
            elementToInsert = FolderElement.insertSententialPath(elementToInsert, subPath);
        }

        return elementToInsert;
    }

    public List<FileElement> getFilesForChildFolder(final String folderName) {
        List<FileElement> resultFiles = null;

        for (final FolderElement folderElement : this.childFolderElements) {
            if (folderElement.getName().equals(folderName)) {
                resultFiles = folderElement.getChildFileElements();
                break;
            }
        }

        ExceptionUtils.throwIfObjectIsNull(resultFiles, folderName);

        return resultFiles;
    }

    public FolderElement getFolder(final String folderName) {
        FolderElement resultFolder = null;

        for (final FolderElement folderElement : this.childFolderElements) {
            if (folderElement.getName().equals(folderName)) {
                resultFolder = folderElement;
                break;
            }
        }

        ExceptionUtils.throwIfObjectIsNull(resultFolder, folderName);

        return resultFolder;
    }

    public FileElement getFile(final String fileName) {
        FileElement resultFile = null;

        for (final FileElement fileElement : this.childFileElements) {
            if (fileElement.getFileName().equals(fileName)) {
                resultFile = fileElement;
                break;
            }
        }

        ExceptionUtils.throwIfObjectIsNull(resultFile, fileName);

        return resultFile;
    }

    public FileElement getFileByExtension(final String extension) {
        FileElement resultFile = null;

        for (final FileElement fileElement : this.childFileElements) {
            if (StringUtils.endsWith(fileElement.getFileName(), extension)
                    && StringUtils.isEmpty(FilenameUtils.getExtension(StringUtils.substringBeforeLast(
                    fileElement.getFileName(), extension)))) {
                resultFile = fileElement;
                break;
            }
        }

        return resultFile;
    }

    public boolean haveChildFolder(final String folderName) {
        boolean result = false;

        for (final FolderElement folderElement : this.childFolderElements) {
            if (folderElement.getName().equals(folderName)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public boolean haveFile(final String fileName) {
        boolean result = false;

        for (final FileElement fileElement : this.childFileElements) {
            if (fileElement.getFileName().equals(fileName)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public List<String> getFolderNames() {
        final List<String> foldersNames = new ArrayList<String>();

        for (final FolderElement folderElement : this.childFolderElements) {
            foldersNames.add(folderElement.getName());
        }

        return foldersNames;
    }

    public List<String> getFileNames() {
        final List<String> filesNames = new ArrayList<String>();

        for (final FileElement fileElement : this.childFileElements) {
            filesNames.add(fileElement.getFileName());
        }

        return filesNames;
    }

}
