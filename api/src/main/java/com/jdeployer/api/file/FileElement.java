/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.file;

/**
 * Represents a file. Contains File Name plus File content.
 *
 * @author mcmartins
 */
public class FileElement {

    private final byte[] fileElementContent;
    private final String fileName;

    /**
     * Default constructor
     *
     * @param fileName           the file name.
     * @param fileElementContent the file content.
     */
    public FileElement(final String fileName, final byte[] fileElementContent) {
        this.fileElementContent = fileElementContent;
        this.fileName = fileName;
    }

    /**
     * Gets {@code fileElementContent} property.
     *
     * @return {@code fileElementContent} value
     */
    public byte[] getFileElementContent() {
        return this.fileElementContent;
    }

    /**
     * Gets {@code fileName} property.
     *
     * @return {@code fileName} value
     */
    public String getFileName() {
        return this.fileName;
    }

}
