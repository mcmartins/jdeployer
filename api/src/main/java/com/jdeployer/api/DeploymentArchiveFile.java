/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api;

import java.io.InputStream;

/**
 * Represents a Deployment Archive file.
 * <T> folder object Type.
 *
 * @author mcmartins
 */
public interface DeploymentArchiveFile<T> {

    /**
     * Root folder name expected on Deployment Archive (.dar) file.
     */
    String DEPLOYMENT_ARCHIVE_FILE_ROOT_FOLDER = "deployment-archive";
    /**
     * Deployment Archive File configuration xml file, expected on Deployment Archive (.dar) file.
     */
    String DEPLOYMENT_ARCHIVE_CONFIGURATION_FILE_NAME = "deployment-archive-configuration.xml";
    /**
     * Chain configuration.
     */
    String DEPLOYMENT_ARCHIVE_CONFIGURATION_KEY = "deployment.chain.config.xml";

    /**
     * Returns the Deployment Archive File Name.
     *
     * @return deploymentArchiveFileName.
     */
    String getFileName();

    /**
     * Returns the Deployment Archive File.
     *
     * @return deploymentArchiveFile.
     */
    InputStream getFileStream();

    /**
     * Returns the Deployment Archive File content.
     *
     * @return deploymentArchiveFileContent.
     */
    T getFileContent();

    /**
     * Returns a Root File based on its name.
     *
     * @param fileName the file name
     * @return the RootFolderFileContent.
     */
    InputStream getRootFolderFileContent(String fileName);

    /**
     * Returns the file content based on its name.
     *
     * @param fileName the file name.
     * @return fileContent.
     */
    InputStream getFileContent(String fileName);

    /**
     * Returns the Deployment Archive File content.
     *
     * @param location the location that should be retrieved
     * @return deploymentArchiveFileContent.
     */
    T getFolder(String location);

    /**
     * Returns the file content based on a path and its name.
     *
     * @param path     the path to the file.
     * @param fileName the file name.
     * @return fileContent.
     */
    InputStream getFileInFolderContent(String path, String fileName);

}
