/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.util;

import com.google.common.base.Preconditions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Deployment utility methods.
 *
 * @author mcmartins
 */
public final class FileUtils {

    /**
     * Default constructor.
     */
    private FileUtils() {
    }

    public static List<URL> loadAllResourcesFromDirectoryInClassPath(final ClassLoader classLoader,
                                                                     final String directory,
                                                                     final String extension)
            throws URISyntaxException, IOException {
        final List<URL> matchFiles = new ArrayList<URL>();
        final URL dirURL = classLoader.getResource(directory);

        if (dirURL != null && "file".equals(dirURL.getProtocol())) {
            final String[] files = new File(dirURL.toURI()).list();
            if (files != null) {
                for (String file : files) {
                    if (file.endsWith(extension)) {
                        matchFiles.add(classLoader.getResource(directory + file));
                    }
                }
            }
        }

        return matchFiles;
    }

    /**
     * Transforms an InputStream into a file to retrieve its URL.
     *
     * @param inputStream the input stream.
     * @return URL the url.
     * @throws MalformedURLException .
     */
    public static URL inputStreamToURL(final InputStream inputStream) throws MalformedURLException {
        Preconditions.checkNotNull(inputStream);

        final File temp = new File("TEMP");
        final int oneMB = 1024;
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(temp);
        } catch (final FileNotFoundException e) {
            return null;
        }

        final byte[] buffer = new byte[oneMB];
        int length;
        try {
            while ((length = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, length);
                fileOutputStream.flush();
            }
        } catch (final IOException ignored) {
        } finally {
            try {
                fileOutputStream.close();
            } catch (final IOException ignored) {
            }
        }
        return temp.toURI().toURL();
    }
}
