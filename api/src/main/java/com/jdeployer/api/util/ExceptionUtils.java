/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.jdeployer.api.util;

import com.jdeployer.api.exception.DeploymentPathNotFoundException;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public final class ExceptionUtils {

    /**
     * Helper to check for null objects.
     *
     * @param object            the object to check for null.
     * @param pathOrFileName the path or file name that originates the object.
     */
    public static void throwIfObjectIsNull(final Object object, final String pathOrFileName) {
        if (object == null) {
            throw new DeploymentPathNotFoundException(null, pathOrFileName);
        }
    }
}
