/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.message;

/**
 * A message base implementation.
 *
 * @author : mcmartins
 */
public abstract class AbstractMessage implements Message {

    private final Message.MessageType messageType;

    private final String message;

    protected AbstractMessage(final Message.MessageType messageType, final String message) {
        this.messageType = messageType;
        this.message = message;
    }

    @Override
    public Message.MessageType getMessageType() {
        return this.messageType;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
