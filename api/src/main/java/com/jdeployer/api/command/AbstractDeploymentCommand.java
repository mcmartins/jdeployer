/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.command;

import com.google.common.base.Preconditions;
import com.jdeployer.api.context.DeploymentContext;
import com.jdeployer.api.exception.DeploymentContextNotFoundException;
import org.apache.commons.chain.Context;
import org.apache.commons.chain.Filter;

/**
 * Handles default implementation for commands execution and post execution.
 *
 * @author mcmartins
 */
public abstract class AbstractDeploymentCommand implements Filter {

    /**
     * Current Deployment Command Name.
     */
    private final String deploymentCommandName;

    /**
     * Default constructor.
     *
     * @param deploymentCommandName deployment name.
     */
    protected AbstractDeploymentCommand(final String deploymentCommandName) {
        this.deploymentCommandName = deploymentCommandName;
    }

    @Override
    public final boolean execute(final Context context) throws Exception {
        Preconditions.checkNotNull(context);

        if (!(context instanceof DeploymentContext)) {
            throw new DeploymentContextNotFoundException();
        }

        final DeploymentContext deploymentContext = (DeploymentContext) context;
        deploymentContext.setLastExecutedCommand(this.getDeploymentCommandName());

        return deploymentContext.getArchive(this.getDeploymentCommandName()) != null
                && this.executeCommand(deploymentContext);
    }

    /**
     * Executes the command.
     *
     * @param deploymentContext the chain context.
     * @return see {@link org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)}.
     * @throws Exception if any exception occurs.
     */
    protected abstract boolean executeCommand(final DeploymentContext deploymentContext) throws Exception;

    @Override
    public final boolean postprocess(final Context context, final Exception e) {
        boolean result = true;
        if (e != null) {
            result = false;
        }
        return result;
    }

    /**
     * Returns the deployment step name.
     *
     * @return deployment step name.
     */
    public final String getDeploymentCommandName() {
        return this.deploymentCommandName;
    }
}
