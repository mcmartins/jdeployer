/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.jdeployer.api.command;

import com.jdeployer.api.context.DeploymentContext;

/**
 * No operation command.
 *
 * @author Manuel Martins
 */
public final class NOPCommand extends AbstractDeploymentCommand {

    /**
     * Default constructor.
     */
    protected NOPCommand() {
        super("NOP-COMMAND");
    }

    @Override
    protected boolean executeCommand(final DeploymentContext deploymentContext) throws Exception {
        return false;
    }

}
