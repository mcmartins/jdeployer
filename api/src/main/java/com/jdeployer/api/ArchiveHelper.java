/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api;

import java.io.InputStream;

/**
 * Represents an archive file/folder. Contains useful methods to handle archive contents.
 *
 * @author Manuel Martins
 */
public interface ArchiveHelper {

    /**
     * Returns a configuration object for the specified type and xsd name, {@code null} if no configuration exists.
     *
     * @return the Archive configuration.
     */
    <T> T getConfiguration(Class<T> type, String xsdName);

    /**
     * Returns the path of the archive, if type is {@link DeploymentArchive.ArchiveType#FOLDER}, {@code null} otherwise.
     *
     * @return the path for the archive.
     */
    String getPath();

    /**
     * Returns the name of the archive, if type is {@link DeploymentArchive.ArchiveType#FILE}, {@link DeploymentArchive.ArchiveType#NONE} or {@link DeploymentArchive.ArchiveType#COMPRESSED}, {@code null} otherwise.
     *
     * @return the name of the archive.
     */
    String getName();

    /**
     * Returns the a 'Folder' of type {@code T}, if type is {@link DeploymentArchive.ArchiveType#FOLDER}, {@code null} otherwise.
     *
     * @return the name of the archive.
     */
    <T> T getFolder();

    /**
     * Returns the stream of the file, if type is {@link DeploymentArchive.ArchiveType#COMPRESSED} or {@link DeploymentArchive.ArchiveType#FILE}, {@code null} otherwise.
     *
     * @return the stream of the fle.
     */
    InputStream getContentStream();

    /**
     * Returns the stream of the file, if type is {@link DeploymentArchive.ArchiveType#FOLDER}, {@code null} otherwise.
     *
     * @return the stream of the fle.
     */
    InputStream getContentStream(String name);
}
