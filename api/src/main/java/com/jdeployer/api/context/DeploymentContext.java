/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.context;

import com.google.common.base.Preconditions;
import com.jdeployer.api.ArchiveHelper;
import com.jdeployer.api.DeploymentArchive;
import com.jdeployer.api.DeploymentListener;
import com.jdeployer.api.message.ErrorMessage;
import com.jdeployer.api.message.InfoMessage;
import com.jdeployer.api.message.Message;
import com.jdeployer.api.message.WarningMessage;
import com.jdeployer.generated.DeploymentArchiveConfiguration;
import org.apache.commons.chain.impl.ContextBase;
import org.apache.commons.collections.CollectionUtils;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

/**
 * Adds support for accessing Deployment Archive files and configuration on the {@link org.apache.commons.chain.Context}
 *
 * @author mcmartins
 */
public class DeploymentContext extends ContextBase {

    private static final String UNCHECKED = "unchecked";

    private final List<DeploymentListener> listeners = new ArrayList<DeploymentListener>();

    /**
     * Returns a {@link DeploymentArchiveConfiguration.Archive} based on a step name.
     *
     * @param deploymentStepName the step name.
     * @return {@link DeploymentArchiveConfiguration.Archive} the archive object.
     * @throws JAXBException if configuration file cannot be unmarshalled.
     */
    public ArchiveHelper getArchive(final String deploymentStepName) throws JAXBException {
        final DeploymentArchive file = get(DeploymentArchive.DEPLOYMENT_ARCHIVE);
        return file != null ? file.getDeploymentArchive(deploymentStepName) : null;
    }

    /**
     * Add a new {@link com.jdeployer.api.message.Message} to the result list.
     *
     * @param message the {@link com.jdeployer.api.message.Message}.
     */
    @SuppressWarnings(UNCHECKED)
    public void addErrorMessage(final String message) {
        final List<Message> list = get(DeploymentArchive.DEPLOYMENT_ARCHIVE_RESULT);
        if (list != null) {
            final Message deployMessage = new ErrorMessage(message);
            list.add(deployMessage);
            this.notifyListener(deployMessage);
        }
    }

    /**
     * Add information message.
     *
     * @param message the information message.
     */
    @SuppressWarnings(UNCHECKED)
    public void addInfoMessage(final String message) {
        final List<Message> list = get(DeploymentArchive.DEPLOYMENT_ARCHIVE_RESULT);
        if (list != null) {
            final Message deployMessage = new InfoMessage(message);
            list.add(deployMessage);
            this.notifyListener(deployMessage);
        }
    }

    /**
     * Add warning message.
     *
     * @param message the warning message.l
     */
    @SuppressWarnings(UNCHECKED)
    public void addWarnMessage(final String message) {
        final List<Message> list = get(DeploymentArchive.DEPLOYMENT_ARCHIVE_RESULT);
        if (list != null) {
            final Message deployMessage = new WarningMessage(message);
            list.add(deployMessage);
            this.notifyListener(deployMessage);
        }
    }

    /**
     * Add new messages to result list.
     *
     * @param messages the list of messages.
     */
    @SuppressWarnings(UNCHECKED)
    public void addDeploymentMessages(final List<Message> messages) {
        final List<Message> list = get(DeploymentArchive.DEPLOYMENT_ARCHIVE_RESULT);
        if (list != null) {
            list.addAll(messages);
            this.notifyListener(messages);
        }
    }

    /**
     * Returns the result deployment messages.
     *
     * @return {@link com.jdeployer.api.message.Message} list of deployment messages.
     */
    @SuppressWarnings(UNCHECKED)
    public List<Message> getMessages() {
        final List<Message> list = get(DeploymentArchive.DEPLOYMENT_ARCHIVE_RESULT);
        return list != null ? list : new ArrayList<Message>();
    }

    /**
     * Returns the last executed command name.
     *
     * @return the last executed command name.
     */
    public String getLastExecutedCommand() {
        return get(DeploymentArchive.DEPLOYMENT_ARCHIVE_LAST_COMMAND_EXECUTED);
    }

    /**
     * Set last executed command.
     *
     * @param commandExecutedName the command commandExecutedName.
     */
    public void setLastExecutedCommand(final String commandExecutedName) {
        put(DeploymentArchive.DEPLOYMENT_ARCHIVE_LAST_COMMAND_EXECUTED, commandExecutedName);
    }

    /**
     * Add new Listener.
     *
     * @param deploymentListener the new listener.
     */
    public void addListener(final DeploymentListener deploymentListener) {
        Preconditions.checkNotNull(deploymentListener);

        this.listeners.add(deploymentListener);
    }

    /**
     * Remove  Listener.
     *
     * @param deploymentListener the listener to remove.
     */
    public void removeListener(final DeploymentListener deploymentListener) {
        Preconditions.checkNotNull(deploymentListener);

        this.listeners.remove(deploymentListener);
    }

    /**
     * Remove all listeners.
     */
    public void removeAllListeners() {
        this.listeners.clear();
    }

    /**
     * Notify a listener that a new message is available.
     *
     * @param message the message.
     */
    private void notifyListener(final Message message) {
        for (final DeploymentListener listener : this.listeners) {
            listener.update(message);
        }
    }

    /**
     * Notify a listener that new messages are available.
     *
     * @param messages the list of messages.
     */
    private void notifyListener(final List<Message> messages) {
        if (CollectionUtils.isNotEmpty(messages)) {
            for (final Message message : messages) {
                this.notifyListener(message);
            }
        }
    }

    @SuppressWarnings(UNCHECKED)
    private <T> T get(final String name) {
        return (T) super.get(name);
    }
}
