/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api;

import com.jdeployer.api.message.Message;

/**
 * Notifier for commands execution. Fires when a message is added to the Context.
 *
 * @author mcmartins
 */
public interface DeploymentListener {

    /**
     * Update listeners with deployment messages.
     *
     * @param message the message.
     */
    void update(Message message);
}
