/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api;

import com.jdeployer.generated.DeploymentArchiveConfiguration;

import javax.xml.bind.JAXBException;
import java.io.InputStream;

/**
 * Represents the Deployment Archive.
 *
 * @author mcmartins
 */
public interface DeploymentArchive {

    /**
     * Deployment Archive key for context.
     */
    String DEPLOYMENT_ARCHIVE = "DeploymentArchive";
    /**
     * Last executed command name.
     */
    String DEPLOYMENT_ARCHIVE_LAST_COMMAND_EXECUTED = "DeploymentArchiveLastCommandExecuted";
    /**
     * Deployment Archive Result key for context.
     */
    String DEPLOYMENT_ARCHIVE_RESULT = "DeploymentArchiveResult";

    /**
     * Type of Archive.
     */
    enum ArchiveType {

        FILE, FOLDER, COMPRESSED, NONE
    }

    /**
     * Checks for the archive version.
     *
     * @return {@code true} if the archive matches the last version, otherwise returns {@code false}.
     */
    boolean isLastVersion();

    /**
     * Returns the {@link DeploymentArchiveFile}.
     *
     * @return the {@link DeploymentArchiveFile}.
     */
    DeploymentArchiveFile getDeploymentArchiveFile();

    /**
     * Returns the name of the last command executed.
     *
     * @return lastCommandExecutedName.
     */
    String getLastExecutedCommand();

    /**
     * Sets the name of the last command executed.
     *
     * @param name the name of the command.
     */
    void setLastExecutedCommand(String name);

    /**
     * Returns the {@link DeploymentArchiveConfiguration} for the current deployment.
     *
     * @return the {@link DeploymentArchiveConfiguration} object.
     * @throws JAXBException if an error occurs when unmarshalling the configuration.
     */
    DeploymentArchiveConfiguration getDeploymentArchiveFileConfiguration() throws JAXBException;

    /**
     * Returns the Chain configuration for deployment.
     *
     * @return returns an {@link InputStream} containing the deployment archive configuration file.
     * @throws JAXBException if an error occurs when unmarshalling the configuration.
     */
    InputStream getDeploymentArchiveChainFileConfiguration() throws JAXBException;

    /**
     * Returns the {@link ArchiveHelper} based on a Command Name.
     *
     * @param deploymentCommandName The Deployment Command Name.
     * @return the Archive configuration see {@link DeploymentArchiveConfiguration.Archive}.
     * @throws JAXBException if an error occurs when unmarshalling the configuration.
     */
    ArchiveHelper getDeploymentArchive(String deploymentCommandName) throws JAXBException;
}
