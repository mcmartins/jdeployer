/*
 *  Copyright 2012 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.jdeployer.api.resource;

import com.java.reuse.core.common.Codable;

/**
 * Error Codes for exceptions.
 *
 * @author mcmartins
 */
public enum ResourceCodes implements Codable<String> {

    /**
     * Lookups a message similar to:
     * <p/>
     * Configuration chain File not found!
     */
    CHAIN_CONFIGURATION_NOT_FOUND("deployment.configuration.0001"),

    /**
     * Lookups a message similar to:
     * <p/>
     * Command {0} configuration is wrong! expecting {1} instead of {2}!
     */
    COMMAND_CONFIGURATION_NOT_FOUND("deployment.configuration.0002"),

    /**
     * Lookups a message similar to:
     * <p/>
     * Command {0} configuration is empty!
     */
    COMMAND_CONFIGURATION_EMPTY("deployment.configuration.0003"),

    /**
     * Lookups a message similar to:
     * <p/>
     * File / Folder with name / path - {0} - does not exists!
     */
    PATH_NOT_FOUND("deployment.configuration.0004"),

    /**
     * Lookups a message similar to:
     * <p/>
     * File / Folder with name / path - {0} - does not exists!
     */
    CONTEXT_NOT_FOUND("deployment.configuration.0005");

    private final String code;

    /**
     * Initializes a new instance of {@code Codes} with the specified error code.
     *
     * @param code The error code for this enumeration value.
     */
    ResourceCodes(final String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
